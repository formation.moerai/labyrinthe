#include <iostream>
#include <conio.h>
#include <stdlib.h> //pour utiliser la fonction exit(0);


#include "plateau.h"

/* #include <stdio.h>
#include <stdlib.h> */
using namespace std;
void demandeChoixNiveau(){
    cout <<"Choisi un niveau"<< endl;
            cout <<"tape 1 pour niveau facile"<< endl;
            cout <<"tape 2 pour niveau intermediaire"<< endl;
            cout <<"tape 3 pour niveau difficile"<< endl;
            cout <<"tape 0 pour arreter le jeu"<< endl;
            
}

void demandeChoixPlateau(){
    cout <<"Choisi un niveau"<< endl;
            cout <<"tape 1 pour jouer avec le plateau A"<< endl;
            cout <<"tape 2 pour jouer avec le plateau B"<< endl;
            cout <<"tape 3 pour jouer avec le plateau C"<< endl;
            cout <<"tape 0 pour arreter le jeu"<< endl;
            
}

void affichageFinale(){
    cout <<"Tu as gagner,Bravo"<< endl;
        /* mettre en pause le programme */
        system("pause");
}

int main(){
    /* variable pour definir le choix du niveau désirer par l'utilisateur */
    int choix=10;
    /* variable pour definir le numero du plateau a utiliser */
    int sousChoix=10;
    
        demandeChoixNiveau();
        cin >> choix;



        /* NIVEAU DEBUTANT */
        if (choix==1){
           
            demandeChoixPlateau();
            cin >> sousChoix;
            /* choix du plateau 1 */
            if (sousChoix==1){
                Plateau plat(5,5,"0100001110010100101100000",2,1,5,4);
                plat.appli();
                affichageFinale();
            }
            /* choix du plateau 2 */
            if (sousChoix==2){
                Plateau plat(5,5,"0100001010010110111000000",2,1,5,3);
                plat.appli();
                affichageFinale();
            }
            /* choix du plateau 3 */
            if (sousChoix==3){
                Plateau plat(5,5,"0100001110010100011000100",2,1,3,5);
                plat.appli();
                affichageFinale();
            }
            /* pour quitter le jeu */
            if (sousChoix==0){
                exit(0);
               
            }
        }
        /* NIVEAU INTERMEDIAIRE */
        if (choix==2){
           
            demandeChoixPlateau();
            cin >> sousChoix;
            /* choix du plateau 1 */
            if (sousChoix==1){
                Plateau plat(7,7,"0100000011111001010100101000010101001011110000000",2,1,7,6);
            plat.appli();
            affichageFinale();
            }
            /* choix du plateau 2 */
            if (sousChoix==2){
                Plateau plat(7,7,"0100000010111001110100101110010101001110110000000",2,1,7,6);
                plat.appli();
                affichageFinale();
            }
            /* choix du plateau 3 */
            if (sousChoix==3){
                Plateau plat(7,7,"0100000011111001101000101110000001001101100000100",2,1,5,7);
                plat.appli();
                affichageFinale();
            }
            /* pour quitter le jeu */
            if (sousChoix==0){
                exit(0);
               
            }
        }

        //NIVEAU DIFFICILE
        if (choix==3){
            Plateau plat(10,10,"0100000000011100111001010110100100010010010101011001111101000111000110010110101001011011110000000000",2,1,10,9);
            plat.appli();
            affichageFinale();
        }
        
         

   
    return 0;
}
