#include <iostream>
#include <conio.h>

/* #include <stdio.h>
#include <stdlib.h> */
using namespace std;
class Plateau {
    private :
    /* variable pour faire la boucle de déplacement du joueur pour cela il */
    bool finished = false ;
    char *plateau;
    /* variable pour définir la taille du plateau */
    int SizeX;
    int SizeY;

    int getFullSize () {
        return SizeX*SizeY;
    }
    /* variable pour les coordonée de position initiale */
    int PosX=0;
    int PosY=0;
    /* variable pour les coordonée de déplacement future */
    int PosFutX=1;
    int PosFutY=1;

    /* variable pour les coordonée de fin */
    int PosFinX=0;
    int PosFinY=0;

    //char plateau[36]={};
    int positionFin= 0 ;
    int positionJoueur= 0 ;
    int positionFutur = 0 ;

    /* declaration pour le deplacement du joueur */
    int ch;

    const int haut= 'z';
    const int bas= 's';
    const int gauche= 'q';
    const int droite= 'd';

    const int hautMaj= 'Z';
    const int basMaj= 'S';
    const int gaucheMaj= 'Q';
    const int droiteMaj= 'D';

    /* code pour deplacer avec les fleches */
    /*  fleche haut   => ch=72
        fleche bas    => ch=80
        fleche gauche => ch=75
        fleche droite => ch=77 */


    


    char saisieJoueur =' ';


    public :
    Plateau(const int NewSizeX, const int NewSizeY,const char* NewTableau,const int DebutX, const int DebutY,const int FinX, const int FinY) {
            SizeX = NewSizeX;
            SizeY = NewSizeY;

            PosFutX=DebutX;
            PosFutY=DebutY;
            //Définition de position initiale du joueur
            PosFinX=DebutX;
            PosFinY=DebutY;
            //Définition de position initiale de la sortie
            PosFinX=FinX;
            PosFinY=FinY;
            
            plateau = (char*)malloc(getFullSize()+1);
            for (int i=0; i<(getFullSize()+1); i++) {
                plateau[i]=NewTableau[i];
            }
        }
    
    int chercherPosition(const int PosX, const int PosY) {

            if(PosY!=-0){
            return ((PosY-1)* SizeX) -1+ PosX;
            }
            else{
              return PosX;  
            }
            
        }
    void AfficherJoueur(int PosX,int PosY) {
            cout << endl;
            
            for (int i=0; i<(getFullSize()+1); i++) {
               
                if (i == chercherPosition(PosX,PosY)) {
                    cout << "J";
                } else {
                    if (plateau[i]=='0') {
                        cout << "X" ;
                    }else if(i == chercherPosition(PosFinX,PosFinY)){
                         cout << "F";
                    }
                else cout << " ";
                }
                
                if (i % SizeX == SizeX -1 ) cout << endl;
            }
        }
    int chercherPositionFin(){
        return chercherPosition(PosFinX,PosFinY);
    }
   

    void appli(){
        
         /* recherche des coordonées de la sortie et transformation en index puis stockage dans positionFin*/ 
            positionFin=chercherPositionFin();
        
       while(finished ==false){

            cout<<"A toi de jouer" << endl;
            /* controle pour afficher les coordonée du joueur*/  
            AfficherJoueur(PosX,PosY); 

            /* controle pour afficher les coordonée du joueur*/ 
            //cout << "X = "<< PosX<<endl;
            //cout << "Y = "<< PosY<<endl; 

            
           
            /* recupération de la saisie de l'utilisateur*/ 
            ch=getch();

            /* effacer l'écran*/ 
            system("cls"); 

            
            //affectation des coordonnée Future selon choix utilisateur
            if(((ch==basMaj)or(ch==bas)or(ch==80))&& plateau[chercherPosition(PosX,PosY+1)]!='0') {
                //positionFutur= chercherPosition(PosFutX,PosFutY);
                PosFutX=PosX;
                PosFutY=PosY+1;
            }
            if((ch==hautMaj)or(ch==haut)or(ch==72)&& plateau[chercherPosition(PosX,PosY-1)]!='0'){
                //positionFutur= chercherPosition(PosFutX,PosFutY);
                PosFutX=PosX;
                PosFutY=PosY-1;
            }
            if((ch==gaucheMaj)or(ch==gauche)or(ch==75)&& plateau[chercherPosition(PosX-1,PosY)]!='0'){
                //positionFutur= chercherPosition(PosFutX,PosFutY);
                PosFutX=PosX-1;
                PosFutY=PosY;
            }
            if((ch==droiteMaj)or(ch==droite)or(ch==77)&& plateau[chercherPosition(PosX+1,PosY)]!='0') {
                //positionFutur= chercherPosition(PosFutX,PosFutY);
                PosFutX=PosX+1;
                PosFutY=PosY;
            }
            
           if((PosFutX==PosFinX) && (PosFutY==PosFinY)){
               finished=true;
           }else{
               PosX=PosFutX;
               PosY=PosFutY;
           }
            
        }
    }
        
     




    
};

