PROJET: Labiryth


------------------------------------------------------------------------------------------------------------------
le but du jeu:
------------------------------------------------------------------------------------------------------------------
le joueur représenter par le caractère J dois sortir du labiryth qui est signaler par le caractère F
------------------------------------------------------------------------------------------------------------------
partie technique:
------------------------------------------------------------------------------------------------------------------
tu as un labiryth matérialiser par des murs en caractère X et des chemins matérialiser par un caractère "Espace"
le joueur est placer a un endroit du labiryth, il est représenter par le caractère J
la sortie est placer a un endroit du labiryth, il est représenter par le caractère F

------------------------------------------------------------------------------------------------------------------
pour ce déplacer:
------------------------------------------------------------------------------------------------------------------
pour ce déplacer à gauche on utilise (flèche gauche) ou (lettre q) ou (lettre Q)
pour ce déplacer à droite on utilise (flèche droite) ou (lettre d) ou (lettreD)
pour ce déplacer en haut on utilise (flèche haut) ou (lettre z) ou (lettre Z)
pour ce déplacer en bas on utilise (flèche bas) ou (lettre s) ou (lettre S)

------------------------------------------------------------------------------------------------------------------
partie jeu:
------------------------------------------------------------------------------------------------------------------
il y a deux plateaux
_1 niveau facile (6 x 6)
_1 niveau intermediaire (10 x 20)