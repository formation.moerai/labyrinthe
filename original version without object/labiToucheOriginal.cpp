#include <iostream>
#include <conio.h>
/* #include <stdio.h>
#include <stdlib.h> */
using namespace std;

void afficherPlateau(char plateau[]){
/* affichage du plateau */
int nbreCol=20;
int taillePlateau=200;

                cout << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << "_" << endl;
                for (int i=0;i<taillePlateau;i++){
                    cout << plateau[i];
                    if (i%nbreCol==(nbreCol-1)){
                        cout << endl;
                    }
                }

}
int chercherPosition(char plateau[]){
    int positionJoueur=0;
    /* CHERCHER LA POSITION DE J */
        for(int index=0;index<200;index++){
            if(plateau[index]=='J'){positionJoueur=index;}
            else{}
        }
    return positionJoueur;
}
int chercherPositionFin(char plateau[]){
    int positionJoueurFin=0;
    /* CHERCHER LA POSITION DE J */
        for(int index=0;index<200;index++){
            if(plateau[index]=='F'){positionJoueurFin=index;}
            else{}
        }
    return positionJoueurFin;
}
int main()
{
bool finished = false ;
char plateau[200]={};
int positionJoueur= 0 ;
int positionFutur = 0 ;

char saisieJoueur =' ';

/* declaration pour le deplacement du joueur */
int ch;

const int haut= 'z';
const int bas= 's';
const int gauche= 'q';
const int droite= 'd';

const int hautMaj= 'Z';
const int basMaj= 'S';
const int gaucheMaj= 'Q';
const int droiteMaj= 'D';

/* code pour deplacer avec les fleches */
/*  fleche haut   => ch=72
    fleche bas    => ch=80
    fleche gauche => ch=75
    fleche droite => ch=77 */


/* intitilaliser plateau avec que des zéros */
/* for(int i=0 ;i<100;i++){
plateau[i]='X';
} */

/* création de mur */
plateau[0] = 'X';
plateau[1] = 'J';
plateau[2] = 'X';
plateau[3] = 'X';
plateau[4] = 'X';
plateau[5] = 'X';
plateau[6] = 'X';
plateau[7] = 'X';
plateau[8] = 'X';
plateau[9] = 'X';
plateau[10] = 'X';
plateau[11] = 'X';
plateau[12] = 'X';
plateau[13] = 'X';
plateau[14] = 'X';
plateau[15] = 'X';
plateau[16] = 'X';
plateau[17] = 'X';
plateau[18] = 'X';
plateau[19] = 'X';

plateau[20] = 'X';
plateau[21] = ' ';
plateau[22] = 'X';
plateau[23] = ' ';
plateau[24] = ' ';
plateau[25] = ' ';
plateau[26] = ' ';
plateau[27] = ' ';
plateau[28] = ' ';
plateau[29] = ' ';
plateau[30] = ' ';
plateau[31] = ' ';
plateau[32] = ' ';
plateau[33] = ' ';
plateau[34] = ' ';
plateau[35] = ' ';
plateau[36] = 'X';
plateau[37] = ' ';
plateau[38] = ' ';
plateau[39] = 'X';

plateau[40] = 'X';
plateau[41] = ' ';
plateau[42] = 'X';
plateau[43] = ' ';
plateau[44] = 'X';
plateau[45] = 'X';
plateau[46] = 'X';
plateau[47] = 'X';
plateau[48] = 'X';
plateau[49] = 'X';
plateau[50] = 'X';
plateau[51] = 'X';
plateau[52] = 'X';
plateau[53] = 'X';
plateau[54] = 'X';
plateau[55] = ' ';
plateau[56] = 'X';
plateau[57] = ' ';
plateau[58] = 'X';
plateau[59] = 'X';

plateau[60] = 'X';
plateau[61] = ' ';
plateau[62] = 'X';
plateau[63] = ' ';
plateau[64] = 'X';
plateau[65] = ' ';
plateau[66] = 'X';
plateau[67] = ' ';
plateau[68] = ' ';
plateau[69] = ' ';
plateau[70] = 'X';
plateau[71] = ' ';
plateau[72] = ' ';
plateau[73] = ' ';
plateau[74] = ' ';
plateau[75] = ' ';
plateau[76] = 'X';
plateau[77] = ' ';
plateau[78] = 'X';
plateau[79] = 'X';

plateau[80] = 'X';
plateau[81] = ' ';
plateau[82] = ' ';
plateau[83] = ' ';
plateau[84] = 'X';
plateau[85] = ' ';
plateau[86] = 'X';
plateau[87] = ' ';
plateau[88] = 'X';
plateau[89] = ' ';
plateau[90] = 'X';
plateau[91] = ' ';
plateau[92] = 'X';
plateau[93] = ' ';
plateau[94] = 'X';
plateau[95] = ' ';
plateau[96] = ' ';
plateau[97] = ' ';
plateau[98] = ' ';
plateau[99] = 'X';

plateau[100] = 'X';
plateau[101] = ' ';
plateau[102] = 'X';
plateau[103] = ' ';
plateau[104] = 'X';
plateau[105] = ' ';
plateau[106] = ' ';
plateau[107] = ' ';
plateau[108] = 'X';
plateau[109] = ' ';
plateau[110] = 'X';
plateau[111] = ' ';
plateau[112] = 'X';
plateau[113] = ' ';
plateau[114] = 'X';
plateau[115] = 'X';
plateau[116] = 'X';
plateau[117] = 'X';
plateau[118] = 'X';
plateau[119] = 'X';

plateau[120] = 'X';
plateau[121] = ' ';
plateau[122] = 'X';
plateau[123] = 'X';
plateau[124] = 'X';
plateau[125] = 'X';
plateau[126] = 'X';
plateau[127] = 'X';
plateau[128] = 'X';
plateau[129] = 'X';
plateau[130] = 'X';
plateau[131] = ' ';
plateau[132] = 'X';
plateau[133] = ' ';
plateau[134] = ' ';
plateau[135] = ' ';
plateau[136] = ' ';
plateau[137] = ' ';
plateau[138] = ' ';
plateau[139] = 'X';

plateau[140] = 'X';
plateau[141] = ' ';
plateau[142] = 'X';
plateau[143] = ' ';
plateau[144] = 'X';
plateau[145] = ' ';
plateau[146] = ' ';
plateau[147] = ' ';
plateau[148] = ' ';
plateau[149] = ' ';
plateau[150] = ' ';
plateau[151] = ' ';
plateau[152] = 'X';
plateau[153] = 'X';
plateau[154] = 'X';
plateau[155] = 'X';
plateau[156] = 'X';
plateau[157] = 'X';
plateau[158] = ' ';
plateau[159] = 'X';

plateau[160] = 'X';
plateau[161] = ' ';
plateau[162] = ' ';
plateau[163] = ' ';
plateau[164] = 'X';
plateau[165] = ' ';
plateau[166] = 'X';
plateau[167] = 'X';
plateau[168] = 'X';
plateau[169] = 'X';
plateau[170] = 'X';
plateau[171] = ' ';
plateau[172] = ' ';
plateau[173] = ' ';
plateau[174] = ' ';
plateau[175] = ' ';
plateau[176] = ' ';
plateau[177] = 'X';
plateau[178] = ' ';
plateau[179] = 'F';

plateau[180] = 'X';
plateau[181] = 'X';
plateau[182] = 'X';
plateau[183] = 'X';
plateau[184] = 'X';
plateau[185] = 'X';
plateau[186] = 'X';
plateau[187] = 'X';
plateau[188] = 'X';
plateau[189] = 'X';
plateau[190] = 'X';
plateau[191] = 'X';
plateau[192] = 'X';
plateau[193] = 'X';
plateau[194] = 'X';
plateau[195] = 'X';
plateau[196] = 'X';
plateau[197] = 'X';
plateau[198] = 'X';
plateau[199] = 'X';


    while(finished ==false){
        int positionFin = chercherPositionFin(plateau) ;
        cout<<"position de fin:" << positionFin << endl;
        
        cout<<"position du joueur:" << positionJoueur << endl;
            afficherPlateau(plateau);
        
            positionJoueur=chercherPosition(plateau) ;   
                
                cout<<"A toi de jouer" << endl;
                
                ch=getch();
                /* cin >> saisieJoueur ; */
                system("cls");

                if(((ch==basMaj)or(ch==bas)or(ch==80))&& plateau[positionJoueur+20]!='X') {
                    positionFutur = positionJoueur + 20 ;
                    plateau[positionFutur] = 'J';
                    plateau[positionJoueur] = ' ';
                }
                if(((ch==hautMaj)or(ch==haut)or(ch==72))&& plateau[positionJoueur-20]!='X'){
                    positionFutur = positionJoueur - 20 ;
                    plateau[positionFutur] = 'J';
                    plateau[positionJoueur] = ' ';
                }
                if(((ch==gaucheMaj)or(ch==gauche)or(ch==75))&& plateau[positionJoueur-1]!='X'){
                    positionFutur = positionJoueur - 1 ;
                    plateau[positionFutur] = 'J';
                    plateau[positionJoueur] = ' ';
                }
                if(((ch==droiteMaj)or(ch==droite)or(ch==77))&& plateau[positionJoueur+1]!='X'){
                    positionFutur = positionJoueur + 1 ;
                    plateau[positionFutur] = 'J';
                    plateau[positionJoueur] = ' ';
                }
                //je suis obliger de faire positionFin -1 sinon on va pas pouvoir terminer le jeu
                //donc sa reviens a faire terminer le jeu on etant dans la case avant l'arrivee
                if(positionJoueur == positionFin-1){ finished = true ; }
    }
    system("cls");
    cout << "tu as gagner le jeu mon Gars"<<endl;
    afficherPlateau(plateau);

/* mettre en pause le programme */
system("pause");
return 0;
}